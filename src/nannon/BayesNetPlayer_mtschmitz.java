package nannon;
/**
 * Copyrighted 2013 by Jude Shavlik.  Maybe be freely used for non-profit educational purposes.
 */

/*
 * A player that simply random chooses from among the possible moves.
 * 
 * NOTE: I (Jude) recommend you COPY THIS TO YOUR PLAYER (NannonPlayer_yourLoginName.java) AND THEN EDIT IN THAT FILE.
 * 
 *       Be sure to change "Random-Move Player" in getPlayerName() to something unique to you!
 */

import java.util.List;
import java.util.TreeMap;

public class BayesNetPlayer_mtschmitz extends NannonPlayer {
	private int winCount;
	private int lossCount;
	private int numCells = NannonGameBoard.getCellsOnBoard(); //number of cells
	private int numPieces = NannonGameBoard.getPiecesPerPlayer(); //max number of pieces
	private int[] safeWin = new int [numPieces + 1];
	private int[] safeLoss = new int [numPieces + 1];
	private int[] homeWin = new int [numPieces + 1];
	private int[] homeLoss = new int [numPieces + 1];
	private int[] toWin = new int[numCells + 1];
	private int[] toLoss = new int[numCells+1];
	private int[] fromWin = new int[numCells+1];
	private int[] fromLoss = new int[numCells+1];
	private int hitWin=1;
	private int hitLoss=1;
	private int primeWin=1;
	private int primeLoss=1;
	private int breakWin=1;
	private int breakLoss=1;

	
	
	/*
	 * A good way to create your players is to edit these methods.  See PlayNannon.java for more details.
	 */

	@Override
	public String getPlayerName() { return "mtschmitz bayes net player"; } // <------------------ choose a name for your player here in your (ranamed) copy of this class (ok to simply use your normal name or your initials, but also consider including your login name so unique).
	
	// Constructors.
	public BayesNetPlayer_mtschmitz() {
		initialize();
		
	}
	public BayesNetPlayer_mtschmitz(NannonGameBoard gameBoard) {
		super(gameBoard);
		initialize();
	}
	
	private void initialize() {
		for(int i = 0; i< numPieces+1; i++){
			safeWin[i]=1;
			safeLoss[i]=1;
			homeWin[i]=1;
			homeLoss[i]=1;
		}
		
		for (int i = 0; i < numCells; i++) {
			toWin[i] = 1;
			toLoss[i] = 1;
			fromWin[i] = 1;
			fromLoss[i] = 1;
		}
	}

	@SuppressWarnings("unused") // This prevents a warning from the "if (false)" below.
	@Override
	public List<Integer> chooseMove(int[] boardConfiguration, List<List<Integer>> legalMoves) {
		double bestRatio=0.0;
		List<Integer> bestMove = legalMoves.get(0);

		// Below is some code you might want to use in your solution.
		//      (a) converts to zero-based counting for the cell locations
		//      (b) converts NannonGameBoard.movingFromHOME and NannonGameBoard.movingToSAFE to NannonGameBoard.cellsOnBoard,
		//          (so you could then make arrays with dimension NannonGameBoard.cellsOnBoard+1)
		//      (c) gets the current and next board configurations.
		
		if ( legalMoves != null) for (List<Integer> move : legalMoves) { // <----- be sure to drop the "false &&" !
			
			int fromCountingFromOne    = move.get(0);  // Convert below to an internal count-from-zero system.
			int   toCountingFromOne    = move.get(1);			
			int                 effect = move.get(2);  // See ManageMoveEffects.java for the possible values that can appear here.	
			
			// Note we use 0 for both 'from' and 'to' because one can never move FROM SAFETY or TO HOME, so we save a memory cell.
			int from = (fromCountingFromOne == NannonGameBoard.movingFromHOME ? 0 : fromCountingFromOne);
			int to   = (toCountingFromOne   == NannonGameBoard.movingToSAFETY ? 0 : toCountingFromOne);
			
			// The 'effect' of move is encoded in these four booleans:
		    boolean        hitOpponent = ManageMoveEffects.isaHit(      effect);  // Did this move 'land' on an opponent (sending it back to HOME)?
		    boolean       brokeMyPrime = ManageMoveEffects.breaksPrime( effect);  // A 'prime' is when two pieces from the same player are adjacent on the board;
		                                                                          // an opponent can NOT land on pieces that are 'prime' - so breaking up a prime of 
		                                                                          // might be a bad idea.
		    boolean extendsPrimeOfMine = ManageMoveEffects.extendsPrime(effect);  // Did this move lengthen (i.e., extend) an existing prime?
		    boolean createsPrimeOfMine = ManageMoveEffects.createsPrime(effect);  // Did this move CREATE a NEW prime? (A move cannot both extend and create a prime.)
		    
		    // Note that you can compute other effects than the four above (but you need to do it from the info in boardConfiguration, resultingBoard, and move).
		    
			// See comments in updateStatistics() regarding how to use these.
			int[] resultingBoard = gameBoard.getNextBoardConfiguration(boardConfiguration, move);  // You might choose NOT to use this - see updateStatistics().
			
			/* Here is what is in a board configuration vector.  There are also accessor functions in NannonGameBoard.java (starts at or around line 60).
			 
			   	boardConfiguration[0] = whoseTurn;        // Ignore, since it is OUR TURN when we play, by definition. (But needed to compute getNextBoardConfiguration.)
        		boardConfiguration[1] = homePieces_playerX; 
        		boardConfiguration[2] = homePieces_playerO;
        		boardConfiguration[3] = safePieces_playerX;
        		boardConfiguration[4] = safePieces_playerO;
        		boardConfiguration[5] = die_playerX;      // I added these early on, but never used them.
        		boardConfiguration[6] = die_playerO;      // Probably can be ignored since get the number of legal moves, which is more meaningful.
       
        		cells 7 to (6 + NannonGameBoard.cellsOnBoard) record what is on the board at each 'cell' (ie, board location).
        					- one of NannonGameBoard.playerX, NannonGameBoard.playerO, or NannonGameBoard.empty.
        		
			 */
			
			double PhitWin;
			double PhitLoss;
			double PprimeWin;
			double PprimeLoss;
			double PbreakWin;
			double PbreakLoss;
			
			if(hitOpponent){
				PhitWin = hitWin/ (double)winCount;
				PhitLoss = hitLoss/ (double)lossCount;
			}else{
				PhitWin = 1.0- (hitWin/ (double)winCount );
				PhitLoss =1.0 - (hitLoss/ (double)lossCount);
			}
			 
			if(extendsPrimeOfMine || createsPrimeOfMine){
				PprimeWin = primeWin/ (double)winCount;
			 	PprimeLoss= primeLoss/ (double)lossCount;
			}else{
				PprimeWin = 1.0 - (primeWin/ (double)winCount );
				PprimeLoss =1.0 - (primeLoss/ (double)lossCount);
			}
			if(brokeMyPrime){
				PbreakWin = breakWin/ (double)winCount;
			 	PbreakLoss= breakLoss/ (double)lossCount;
			}else{
				PbreakWin = 1.0- (breakWin/ (double)winCount );
				PbreakLoss =1.0 - (breakLoss/ (double)lossCount);
			}
			
			double hitRatio = PhitWin/PhitLoss;
			double primeRatio =PprimeWin/PprimeLoss;
			double breakRatio =PbreakWin/PbreakLoss;
			 
			 
			double PsafeWin = safeWin[resultingBoard[3]] / (double)winCount;
			double PsafeLoss = safeLoss[resultingBoard[3]] / (double)lossCount;
			double ratioSafe = PsafeWin/PsafeLoss;

			double PhomeWin = homeWin[resultingBoard[1]] / (double)winCount;
			double PhomeLoss = homeLoss[resultingBoard[1]] / (double)lossCount;
			double ratioHome = PhomeWin/PhomeLoss;
			
			double PtoWin = toWin[to]/(double)winCount;
			double PtoLoss = toLoss[to]/(double)lossCount;
			double toRatio = PtoWin/PtoLoss;
			
			double PfromWin = fromWin[from]/(double)winCount;
			double PfromLoss = fromLoss[from]/(double)lossCount;
			double fromRatio = PfromWin/PfromLoss;
			
			double Pwin = (double) winCount / (winCount + lossCount);
			double Ploss =(double) lossCount / (winCount+ lossCount); 
			double WLratio = Pwin/Ploss;
			
			//full calculation of liklihood of winning from prospective move using Naive Bayes
			double moveRatio = ratioSafe*ratioHome*hitRatio*toRatio*fromRatio*WLratio;

			if(moveRatio > bestRatio){
				bestMove = move;
				bestRatio = moveRatio;
			}
		}
		return bestMove;
	}

	@Override
	public void updateStatistics(boolean             didIwinThisGame, 
		                         List<int[]>         allBoardConfigurationsThisGameForPlayer,
			                     List<Integer>       allCountsOfPossibleMovesForPlayer,
			                     List<List<Integer>> allMovesThisGameForPlayer) {
		
		// Do nothing with these in the random player (but hints are here for use in your players).	
		
		// However, here are the beginnings of what you might want to do in your solution (see comments in 'chooseMove' as well).
			int numberOfMyMovesThisGame = allBoardConfigurationsThisGameForPlayer.size();	
			
			for (int myMove = 0; myMove < numberOfMyMovesThisGame; myMove++) {
				int[]         currentBoard        = allBoardConfigurationsThisGameForPlayer.get(myMove);
				int           numberPossibleMoves = allCountsOfPossibleMovesForPlayer.get(myMove);
				List<Integer> moveChosen          = allMovesThisGameForPlayer.get(myMove);
				int[]         resultingBoard      = (numberPossibleMoves < 1 ? currentBoard // No move possible, so board is unchanged.
						                                                     : gameBoard.getNextBoardConfiguration(currentBoard, moveChosen));
				
				// You should compute the statistics needed for a Bayes Net for any of these problem formulations:
				//
				//     prob(win | currentBoard and chosenMove and chosenMove's Effects)  <--- this is what I (Jude) did, but mainly because at that point I had not yet written getNextBoardConfiguration()
				//     prob(win | resultingBoard and chosenMove's Effects)               <--- condition on the board produced and also on the important changes from the prev board
				//
				//     prob(win | currentBoard and chosenMove)                           <--- if we ignore 'chosenMove's Effects' we would be more in the spirit of a State Board Evaluator (SBE)
				//     prob(win | resultingBoard)                                        <--- but it seems helpful to know something about the impact of the chosen move (ie, in the first two options)
				//
				//     prob(win | currentBoard)                                          <--- if you estimate this, be sure when CHOOSING moves you apply to the NEXT boards (since when choosing moves, one needs to score each legal move).
				
				if (numberPossibleMoves <= 1) { continue; } // DO NOT LEARN IF THERE IS A FORCED MOVE
	
				// Convert to our internal count-from-zero system.
				// A move is a list of three integers.  Their meanings should be clear from the variable names below.
				int fromCountingFromOne = moveChosen.get(0);  // Convert below to an internal count-from-zero system.
				int   toCountingFromOne = moveChosen.get(1);
				int              effect = moveChosen.get(2);  // See ManageMoveEffects.java for the possible values that can appear here. Also see the four booleans below.

				// Note we use 0 for both 'from' and 'to' because one can never move FROM SAFETY or TO HOME, so we save a memory cell.
				int from = (fromCountingFromOne == NannonGameBoard.movingFromHOME ? 0 : fromCountingFromOne);
				int to   = (toCountingFromOne   == NannonGameBoard.movingToSAFETY ? 0 : toCountingFromOne);
				
				// The 'effect' of move is encoded in these four booleans:
			    boolean        hitOpponent = ManageMoveEffects.isaHit(      effect); // Explained in chooseMove() above.
			    boolean       brokeMyPrime = ManageMoveEffects.breaksPrime( effect);
			    boolean extendsPrimeOfMine = ManageMoveEffects.extendsPrime(effect);
			    boolean createsPrimeOfMine = ManageMoveEffects.createsPrime(effect);
				
			    if (didIwinThisGame) {
					safeWin[resultingBoard[3]]++;
					homeWin[resultingBoard[1]]++;
					toWin[to]++;
					fromWin[from]++;
					if(hitOpponent)hitWin++;
					if(extendsPrimeOfMine && createsPrimeOfMine)primeWin++;
					if(brokeMyPrime)breakWin++;
					winCount++;
				}
				else{
					safeLoss[resultingBoard[3]]++;
					homeLoss[resultingBoard[1]]++;
					toLoss[to]++;
					fromLoss[from]++;
					if(hitOpponent)	hitLoss++;
					if(extendsPrimeOfMine && createsPrimeOfMine)primeLoss++;
					if(brokeMyPrime)breakLoss++;
					lossCount++;
				}			
			}
		}
	
	/**
	 * This is a method that finds the maximum integer value in an array of integers
	 * 
	 * @param array a is the array for which the max value is to be found
	 * @return the maximum value of the array
	 */
	

	private static int maxValue(int[] a) {
		int max = a[0];
		for (int ktr = 0; ktr < a.length; ktr++) {
			if (a[ktr] > max) {
				max = a[ktr];
			}
		}
		return max;
	}
	
	/**
	 * The report learned model method prints a number of statistics about the features that were
	 * used to learn
	 * 
	 */
	
	@Override
	public void reportLearnedModel() { 
		
		TreeMap<Double, String> t = new TreeMap<Double, String>();
		int safeWinBest = maxValue(safeWin);
		int safeLossWorst = maxValue(toWin);
		int toWinBest = maxValue(fromWin);
		int toLossWorst = maxValue(safeLoss);
		int fromWinBest = maxValue(toLoss);
		int fromLossWorst = maxValue(fromLoss);
		
		t.put( ((double)hitWin/winCount)/(((double)hitLoss/lossCount)), "Move is a Hit");
		t.put( ((double)primeWin/winCount)/(((double)primeLoss/lossCount)), "Move makes a prime");
		t.put( ((double) breakWin/winCount)/(((double)breakLoss/lossCount)), "Move breaks your prime");
		t.put( (1.0 -(double)hitWin/winCount)/(1.0 -((double)hitLoss/lossCount)), "Move is not a Hit");
		t.put( (1.0 -(double)primeWin/winCount)/(1.0 -((double)primeLoss/lossCount)), "Move doesn't makes a prime");
		t.put( (1.0 - (double) breakWin/winCount)/(1.0- ((double)breakLoss/lossCount)), "Move doesn't break your prime");
		t.put( ((double)safeWinBest/winCount)/(((double)safeLossWorst/lossCount)), "Safe");
		t.put( ((double)toWinBest/winCount)/(((double)toLossWorst/lossCount)), "To");
		t.put( ((double)fromWinBest/winCount)/(((double)fromLossWorst/lossCount)), "From");

		
		Utils.println("\n-------------------------------------------------");
		Utils.println("\n Hi, I'm (" + getPlayerName() + ") and I learned:");
		Utils.println("The feature most indicative of Losing was: " + t.firstEntry().getValue() 
				+ "\nWith an odds value of " + t.firstKey().doubleValue() 
				+ "\nThe feature most indicative of Winning was " + t.lastEntry().getValue()
				+ "\nWith an odds value of  " + t.lastKey().doubleValue());
				t.pollFirstEntry();
				t.pollFirstEntry();
				t.pollFirstEntry();
				t.pollFirstEntry();
	    Utils.println("\nThe feature most uselsess was " +  t.pollFirstEntry() );

		Utils.println("\n-------------------------------------------------");		
	}
	
	
}