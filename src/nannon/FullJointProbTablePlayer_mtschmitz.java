package nannon;


/**
 * Copyrighted 2013 by Jude Shavlik.  Maybe be freely used for non-profit educational purposes.
 * mtschmitz HW 3
 */

/*
 * A player that simply random chooses from among the possible moves.
 * 
 * NOTE: I (Jude) recommend you COPY THIS TO YOUR PLAYER (NannonPlayer_yourLoginName.java) AND THEN EDIT IN THAT FILE.
 * 
 *       Be sure to change "Random-Move Player" in getPlayerName() to something unique to you!
 */

import java.util.List;
import java.util.Arrays;

public class FullJointProbTablePlayer_mtschmitz extends NannonPlayer {
	private int winCount; //games won
	private int loseCount; //games lost
	private int posSafe;  //number possible safe
	private int posCell;  //number possible per cell
	private int posHome;  //number possible at home
	private int[][][][][][][][][][] fullJointWin; // joint probability table of boards that were part of winning game
	private int[][][][][][][][][][] fullJointLose; // joint probability table of boards that were part of losing game
	
	@Override
	public String getPlayerName() { return "MTSchmitz Fully Jointed"; }
	
	// Constructors.
	public FullJointProbTablePlayer_mtschmitz() {
		initialize();
		
	}
	public FullJointProbTablePlayer_mtschmitz(NannonGameBoard gameBoard) {
		super(gameBoard);
		initialize();
	}
	
	private void initialize() {
		winCount=0;
		loseCount=0;
		posSafe = NannonGameBoard.getPiecesPerPlayer() + 1;//+1 because can be empty
		posHome = NannonGameBoard.getPiecesPerPlayer() + 1;
		posCell = 3; //Can either be empty, X or O
		fullJointWin = new int[posSafe][posHome][posCell][posCell][posCell][posCell][posCell][posCell][posHome][posSafe];
		fullJointLose = new int[posSafe][posHome][posCell][posCell][posCell][posCell][posCell][posCell][posHome][posSafe];
		fullJointWin = fullaOnes(fullJointWin);
		fullJointLose = fullaOnes(fullJointLose);
	}
	
	/**
	 * fullaOnes fills a 10D int array with 1
	 * 
	 * @param a the array to fill with ones
	 * @return a the array full of ones
	 */
	
	private int[][][][][][][][][][] fullaOnes(int[][][][][][][][][][] a){
		for (int [][][][][][][][][] b : a){
			for (int [][][][][][][][] c : b) {
				for (int [][][][][][][] d :c) {
					for (int [][][][][][] e : d) {
						for (int [][][][][] f : e) {
							for (int [][][][] g : f) {
								for (int [][][] h : g) {
									for (int [][] i :h) {
										for (int [] j : i) {
											Arrays.fill( j , 1);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return a;
	}
	

	@SuppressWarnings("unused") // This prevents a warning from the "if (false)" below.
	@Override
	public List<Integer> chooseMove(int[] boardConfiguration, List<List<Integer>> legalMoves) {
		
		// Below is some code you might want to use in your solution.
		//      (a) converts to zero-based counting for the cell locations
		//      (b) converts NannonGameBoard.movingFromHOME and NannonGameBoard.movingToSAFE to NannonGameBoard.cellsOnBoard,
		//          (so you could then make arrays with dimension NannonGameBoard.cellsOnBoard+1)
		//      (c) gets the current and next board configurations.
		List<Integer> bestMove = null;
		double bestRatio=0.0;

		if (legalMoves != null) for (List<Integer> move : legalMoves) { // <----- be sure to drop the "false &&" !
	//		if (legalMoves == null) continue;
			
			int fromCountingFromOne    = move.get(0);  // Convert below to an internal count-from-zero system.
			int   toCountingFromOne    = move.get(1);			
			int                 effect = move.get(2);  // See ManageMoveEffects.java for the possible values that can appear here.	
			
			// Note we use 0 for both 'from' and 'to' because one can never move FROM SAFETY or TO HOME, so we save a memory cell.
			int from = (fromCountingFromOne == NannonGameBoard.movingFromHOME ? 0 : fromCountingFromOne);
			int to   = (toCountingFromOne   == NannonGameBoard.movingToSAFETY ? 0 : toCountingFromOne);
			
			// The 'effect' of move is encoded in these four booleans:
		    boolean        hitOpponent = ManageMoveEffects.isaHit(      effect);  // Did this move 'land' on an opponent (sending it back to HOME)?
		    boolean       brokeMyPrime = ManageMoveEffects.breaksPrime( effect);  // A 'prime' is when two pieces from the same player are adjacent on the board;
		                                                                          // an opponent can NOT land on pieces that are 'prime' - so breaking up a prime of 
		                                                                          // might be a bad idea.
		    boolean extendsPrimeOfMine = ManageMoveEffects.extendsPrime(effect);  // Did this move lengthen (i.e., extend) an existing prime?
		    boolean createsPrimeOfMine = ManageMoveEffects.createsPrime(effect);  // Did this move CREATE a NEW prime? (A move cannot both extend and create a prime.)
		    
		    // Note that you can compute other effects than the four above (but you need to do it from the info in boardConfiguration, resultingBoard, and move).
		    
			// See comments in updateStatistics() regarding how to use these.
			int[] resultingBoard = gameBoard.getNextBoardConfiguration(boardConfiguration, move);  // You might choose NOT to use this - see updateStatistics().
			
			/* Here is what is in a board configuration vector.  There are also accessor functions in NannonGameBoard.java (starts at or around line 60).
			 
			   	boardConfiguration[0] = whoseTurn;        // Ignore, since it is OUR TURN when we play, by definition. (But needed to compute getNextBoardConfiguration.)
        		boardConfiguration[1] = homePieces_playerX; 
        		boardConfiguration[2] = homePieces_playerO;
        		boardConfiguration[3] = safePieces_playerX;
        		boardConfiguration[4] = safePieces_playerO;
        		boardConfiguration[5] = die_playerX;      // I added these early on, but never used them.
        		boardConfiguration[6] = die_playerO;      // Probably can be ignored since get the number of legal moves, which is more meaningful.
       
        		cells 7 to (6 + NannonGameBoard.cellsOnBoard) record what is on the board at each 'cell' (ie, board location).
        					- one of NannonGameBoard.playerX, NannonGameBoard.playerO, or NannonGameBoard.empty.
        		
			 */
			
			//P(Win | Potential state)/P(Lose| Potential State)
			//Simplification, see report for expansion of probability equation
			double winCurrent = fullJointWin[resultingBoard[3]][resultingBoard[1]][resultingBoard[7]][boardConfiguration[8]][boardConfiguration[9]][boardConfiguration[10]][boardConfiguration[11]][boardConfiguration[12]][resultingBoard[2]][resultingBoard[4]]; //[Safe][Home][Cells1-6][Home][Safe]
			double lossCurrent = fullJointLose[resultingBoard[3]][resultingBoard[1]][resultingBoard[7]][boardConfiguration[8]][boardConfiguration[9]][boardConfiguration[10]][boardConfiguration[11]][boardConfiguration[12]][resultingBoard[2]][resultingBoard[4]];
			winCurrent=(winCurrent/(double) winCount)*((double)winCount/((double)winCount+(double)loseCount));
		    lossCurrent=(lossCurrent/(double) winCount)*((double)loseCount/((double)winCount+(double)loseCount));

			//ratio of the probabilities of winning and losing, choose move with the best ratio
			double ratio = winCurrent/lossCurrent;
			
			if(ratio > bestRatio){
				bestMove = move;
				bestRatio = ratio;
			}		
		}
	return bestMove;
	}

	@SuppressWarnings("unused") // This prevents a warning from the "if (false)" below.
	@Override
	//record data to learn
	public void updateStatistics(boolean             didIwinThisGame, 
		                         List<int[]>         allBoardConfigurationsThisGameForPlayer,
			                     List<Integer>       allCountsOfPossibleMovesForPlayer,
			                     List<List<Integer>> allMovesThisGameForPlayer) {
		
		// Do nothing with these in the random player (but hints are here for use in your players).	
		
		// However, here are the beginnings of what you might want to do in your solution (see comments in 'chooseMove' as well).
			int numberOfMyMovesThisGame = allBoardConfigurationsThisGameForPlayer.size();	
			
			for (int myMove = 0; myMove < numberOfMyMovesThisGame; myMove++) {
				int[]         currentBoard        = allBoardConfigurationsThisGameForPlayer.get(myMove);
				int           numberPossibleMoves = allCountsOfPossibleMovesForPlayer.get(myMove);
				List<Integer> moveChosen          = allMovesThisGameForPlayer.get(myMove);
				int[]         resultingBoard      = (numberPossibleMoves < 1 ? currentBoard // No move possible, so board is unchanged.
						                                                     : gameBoard.getNextBoardConfiguration(currentBoard, moveChosen));	
				
				
				
				// You should compute the statistics needed for a Bayes Net for any of these problem formulations:
				//
				//     prob(win | currentBoard and chosenMove and chosenMove's Effects)  <--- this is what I (Jude) did, but mainly because at that point I had not yet written getNextBoardConfiguration()
				//     prob(win | resultingBoard and chosenMove's Effects)               <--- condition on the board produced and also on the important changes from the prev board
				//
				//     prob(win | currentBoard and chosenMove)                           <--- if we ignore 'chosenMove's Effects' we would be more in the spirit of a State Board Evaluator (SBE)
				//     prob(win | resultingBoard)                                        <--- but it seems helpful to know something about the impact of the chosen move (ie, in the first two options)
				//
				//     prob(win | currentBoard)                                          <--- if you estimate this, be sure when CHOOSING moves you apply to the NEXT boards (since when choosing moves, one needs to score each legal move).
				
				//Do not learn if it is a forced move, improves runtime
				if (numberPossibleMoves <= 1) { continue; } // If NO moves possible, nothing to learn from (it is up to you if you want to learn for cases where there is a FORCED move, ie only one possible move).
	
				// Convert to our internal count-from-zero system.
				// A move is a list of three integers.  Their meanings should be clear from the variable names below.
				int fromCountingFromOne = moveChosen.get(0);  // Convert below to an internal count-from-zero system.
				int   toCountingFromOne = moveChosen.get(1);
				int              effect = moveChosen.get(2);  // See ManageMoveEffects.java for the possible values that can appear here. Also see the four booleans below.

				// Note we use 0 for both 'from' and 'to' because one can never move FROM SAFETY or TO HOME, so we save a memory cell.
				int from = (fromCountingFromOne == NannonGameBoard.movingFromHOME ? 0 : fromCountingFromOne);
				int to   = (toCountingFromOne   == NannonGameBoard.movingToSAFETY ? 0 : toCountingFromOne);
				
				// The 'effect' of move is encoded in these four booleans:
			    boolean        hitOpponent = ManageMoveEffects.isaHit(      effect); // Explained in chooseMove() above.
			    boolean       brokeMyPrime = ManageMoveEffects.breaksPrime( effect);
			    boolean extendsPrimeOfMine = ManageMoveEffects.extendsPrime(effect);
			    boolean createsPrimeOfMine = ManageMoveEffects.createsPrime(effect);
				
			    //record the world state for winning or losing moves depending on outcome of game
				if (didIwinThisGame){
					fullJointWin[resultingBoard[3]][resultingBoard[1]][resultingBoard[7]][resultingBoard[8]][resultingBoard[9]][resultingBoard[10]][resultingBoard[11]][resultingBoard[12]][resultingBoard[2]][resultingBoard[4]]++;
					winCount += numberOfMyMovesThisGame + 1;
				}else { 
					fullJointLose[resultingBoard[3]][resultingBoard[1]][resultingBoard[7]][resultingBoard[8]][resultingBoard[9]][resultingBoard[10]][resultingBoard[11]][resultingBoard[12]][resultingBoard[2]][resultingBoard[4]]++;
					loseCount += numberOfMyMovesThisGame + 1;
				}
			}
		}

	
	@Override
	public void reportLearnedModel() { // You can add some code here that reports what was learned, eg the most important feature for WIN and for LOSS.  And/or all the weights on your features.
		Utils.println("\n-------------------------------------------------");
		Utils.println("\nI (" + getPlayerName() + ") learned which board states are the best and worst");		
		Utils.println("\n-------------------------------------------------");
	}
}
