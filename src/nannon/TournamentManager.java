package nannon;

/*
*Copyrighted 2013 by Jude Shavlik.  Maybe be freely used for non-profit educational purposes.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author shavlik
 *
 */
public class TournamentManager {

	/**
	 * Students are welcome to use this method to debug code, run tests, etc.  But it isn't necessary.
	 * (It is the initial version of the code that manages class-wide tournaments.  It does larger-scale debugging.)
	 */
	
	public static void main(String[] args) {		

		String[] players = {  "random"
							, "greedyHandCoded" 
							};
		
		int minI = 0; // This allows a given run to only use SOME players for Player X.  PlayerO always ranges across the items in the list 'players'.
		int maxI = players.length;
		
		long playerXwins = 0;
		long playerOwins = 0;

		int roundsToPlay = 1;
		
		NannonGameBoard.setPiecesPerPlayer(5); // 3; // If too large, boards can get into a deadlock state.
		NannonGameBoard.setCellsOnBoard(12); //2 * NannonGameBoard.piecesPerPlayer; // TODO - some bugs if this is less than sidesOnDice.
//		NannonGameBoard.sidesOnDice     = 6; // If this is too small, boards can get into a deadlock state.  If less than 6, some code might crash.  THIS IS NOW A CONSTANT AND WON'T BE CHANGED.

		Nannon.setNumberOfGamesInBurnInPhase(1000000);
		Nannon.setGamesToPlay(1000000 + Nannon.getNumberOfGamesInBurnInPhase()); // Play 1M games AFTER the burn-in (since WINS/LOSES are not counted until burn-in over).
		Nannon.setPrintProgress(false);
		Nannon.setReportLearnedModels(false);
		Nannon.setWaitAfterEachGame(false);
		Nannon.setWaitAfterEachMove(false);
				
		Utils.closeDribbleFile();
		Utils.createDribbleFile("dribbleFiles/" + (minI == maxI ? players[minI] : "all_vs_all") + "_" + (roundsToPlay) + "_rounds_of_"
									+ ((Nannon.getGamesToPlay() - Nannon.getNumberOfGamesInBurnInPhase()) / 1000000) + "M_games"
									+ "_" + (Nannon.getNumberOfGamesInBurnInPhase() / 1000) + "K_burnInGames"
									+ (NannonGameBoard.getPiecesPerPlayer() != 3                                     ? "_" + NannonGameBoard.getPiecesPerPlayer() + "_piecesPerPlayer" : "")
									+ (NannonGameBoard.getPiecesPerPlayer() * 2 != NannonGameBoard.getCellsOnBoard() ? "_" + NannonGameBoard.getCellsOnBoard()    + "_boardSize"       : "")
//									+ (NannonGameBoard.sidesOnDice     != 6                                ? "_" + NannonGameBoard.sidesOnDice     + "_sidedDice"       : "")
									+ ".txt");
		
		long totalRunTime[]   = new long[players.length];
		long wins[  ]         = new long[players.length];
		long losses[]         = new long[players.length];
		long draws[ ]         = new long[players.length]; // Won't happen unless we use very wide boards.
		
		long winsMatrix[  ][] = new long[players.length][players.length];
		long lossesMatrix[][] = new long[players.length][players.length];
		long drawsMatrix[ ][] = new long[players.length][players.length];
		
		Utils.println("\nOut of " + Utils.comma(players.length) + " players: ");
		for (int i = 0; i < players.length; i++) {
			Utils.println(" Need to test " + players[i]);
		}// Utils.waitForEnter();
		
		for (int i = 0; i < players.length; i++) { // I don't think this is necessary in Java, but zero-out everything regardless.
			wins[  i] = 0;
			losses[i] = 0;
			draws[i]  = 0;
			for (int j = 0; j < players.length; j++) {
				winsMatrix[  i][j] = 0;
				lossesMatrix[i][j] = 0;
				drawsMatrix[ i][j] = 0;
			}
		}
		
		int rounds       =   0;
		long startTime = System.currentTimeMillis();
		while (rounds++ < roundsToPlay) {
			for (int i = 0; i < players.length; i++) 
				if (i >= minI && i <= maxI) 
					for (int j = 0; j < players.length; j++) 
						if (i != j) {

				long currentTimeInMilliseconds = System.currentTimeMillis(); // See how we have been playing the game.				
				
				// Print for both PlayerX = i and PlayerO = j and vice versa, so we can watch for asymmetries, which might indicate a bug.
				Utils.println("\n-------------------------------------------------------------------------------------------------\n Round #" + Utils.comma(rounds) + 
						      ":  " + players[i] + " (" + Utils.truncate(wins[i] > 0 ? 100.0 * wins[i] / (wins[i] + losses[i] + draws[i]) : 0, 2) + "% wins) vs. " + 
						              players[j] + " (" + Utils.truncate(wins[j] > 0 ? 100.0 * wins[j] / (wins[j] + losses[j] + draws[j]) : 0, 2) + "% wins).   "  + 
						              "Have been running " + Utils.convertMillisecondsToTimeSpan(currentTimeInMilliseconds - startTime) + "." +
						              (winsMatrix[i][j] > 0 ? "\n    '" + players[i] + "' beats    '" + players[j] + "' " + Utils.truncate(100.0 *   winsMatrix[i][j] / (winsMatrix[i][j] + lossesMatrix[i][j] + drawsMatrix[i][j]), 2) + "% of the time." : "") +
						              // Check for symmetry.
						              (winsMatrix[j][i] > 0 ? "\n    '" + players[j] + "' loses to '" + players[i] + "' " + Utils.truncate(100.0 * lossesMatrix[j][i] / (winsMatrix[j][i] + lossesMatrix[j][i] + drawsMatrix[j][i]), 2) + "% of the time." : "") +
							  "\n-------------------------------------------------------------------------------------------------\n");
								
				int[] results = Nannon.playGames(players[i], players[j]);
				long delta =  System.currentTimeMillis() - currentTimeInMilliseconds;
				totalRunTime[i] += delta; // Record time spent in this match.
				totalRunTime[j] += delta;
				
				wins[  i] += results[0]; // Wins for Player X is in [0].
				losses[j] += results[0];
				wins[  j] += results[1]; // Wins for Player O is in [1].
				losses[i] += results[1];
				draws[ i] += results[2];
				draws[ j] += results[2];
				
				winsMatrix[  i][j] += results[0];
				lossesMatrix[i][j] += results[1];
				drawsMatrix[ i][j] += results[2];
				
				playerXwins += results[0];
				playerOwins += results[1];
				
				if (results[2] > 0)  { Utils.waitForEnter("There have been " + Utils.comma(results[2]) + " draws!"); }
			}			
		}
		Utils.println(                   "\n--------------------------------------------------------------------------\nThe Results of " + 
						Utils.comma(rounds - 1) + " Rounds of " + Utils.comma(Nannon.getGamesToPlay()) + 
						" Games (including " + Utils.comma(Nannon.getNumberOfGamesInBurnInPhase()) 
						+ " burn-in games)\n--------------------------------------------------------------------------\n");
		
		// Sort by WIN percentage.
		List<Double> winsPercentageSorted = new ArrayList<Double>(players.length);
		for (int i = 0; i < players.length; i++) { winsPercentageSorted.add(wins[i] > 0.0 ? 100.0 * wins[i] / (wins[i] + losses[i] + draws[i]) : -1.0); }
		Collections.sort(winsPercentageSorted, Collections.reverseOrder());
		
		boolean[][] marked = new boolean[2][players.length]; 
		
		for (int pass = 0; pass < 2; pass++) { // In the first pass, just report rank and winning percentage.
			for (int j = 0 ; j < players.length; j++) {
				double target = winsPercentageSorted.get(j);
			
				for (int i = 0; i < players.length; i++) if (i >= minI && i <= maxI && !marked[pass][i] && Math.abs(target - (100.0 * wins[i] / (wins[i] + losses[i] + draws[i]))) < 0.01) { // If within one part in 10K (recall we multiplied by 100 already), call it a tie
					// NOTE: If NOT round-robin, the Ranking can be misleading.
					Utils.println("\n   Ranked #" + Utils.padLeft(j + 1, 2) + ", wins =" + Utils.padLeft(wins[i], 9) + " and losses =" + Utils.padLeft(losses[i], 9) 
									+ " (" + Utils.truncate(100.0 * wins[i] / (wins[i] + losses[i] + draws[i]), 2) + "% wins)"
									+ " for: '" + players[i] + "'   Mean runtime per 1M games: " + Utils.convertMillisecondsToTimeSpan( (totalRunTime[i] / (Math.max(1, ((wins[i] + losses[i] + draws[i]) / 1000)))) * 1000)); // If we do "per game" it might be less than 1msec per game, so 'pre-divide' part of the 1M, ie, that "/1000" is 'really" a multiplier.
					
					if (pass > 0) for (int k = 0; k < players.length; k++) if (k != i) {
						if (winsMatrix[  i][k] > 0) Utils.println("      won " + Utils.truncate(100.0 *   winsMatrix[i][k] / (winsMatrix[i][k] + lossesMatrix[i][k] + drawsMatrix[i][k]), 2) + "% of the time against '" + players[k] + "' with mean runtime per 1M games of " + Utils.convertMillisecondsToTimeSpan(1000 * totalRunTime[k] / ((wins[k] + losses[k] + draws[k]) / 1000)) + ".");
						if (lossesMatrix[k][i] > 0) Utils.println("      won " + Utils.truncate(100.0 * lossesMatrix[k][i] / (winsMatrix[k][i] + lossesMatrix[k][i] + drawsMatrix[k][i]), 2) + "% of the time against '" + players[k] + "' as Player O");
					}
					
					marked[pass][i] = true; // Because we have a tolerance in rankings, don't report more than once.
				}
			}
		}
		
		Utils.println("\nDONE!");
			
	}

}
